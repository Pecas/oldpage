// app.js

// define our application and pull in ngRoute and ngAnimate
var pauApp = angular.module('pauApp ', ['ngRoute', 'ngAnimate', 'ngMaterial']);

// THEME ===============================================
pauApp.config(function($mdThemingProvider) {

  $mdThemingProvider.theme('default')
    .primaryPalette('grey', {
      'default': '400', // by default use shade 400 from the pink palette for primary intentions
      'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
      'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
      'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
    })
    // If you specify less than all of the keys, it will inherit from the
    // default shades
   

});

// ROUTING ===============================================
// set our routing for this application
// each route will pull in a different controller
pauApp.config(function($routeProvider) {

    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'components/home/homeView.html',
            controller: 'homeController'
        })
         // about page
        .when('/about', {
            templateUrl: 'components/about/aboutView.html',
            controller: 'aboutController'
        })

        // meetme page
        .when('/meetme', {
            templateUrl: 'components/meetme/meetmeView.html',
            controller: 'meetmeController'
        })
        // mappd page
        .when('/mappd', {
            templateUrl: 'components/mappd/mappdView.html',
            controller: 'mappdController'
        })

         // scheduleme page
        .when('/scheduleme', {
            templateUrl: 'components/scheduleme/schedulemeView.html',
            controller: 'schedulemeController'
        })
         // caria page
        .when('/caria', {
            templateUrl: 'components/caria/cariaView.html',
            controller: 'cariaController'
        })

         // bloody page
        .when('/bloody', {
            templateUrl: 'components/bloody/bloodyView.html',
            controller: 'bloodyController'
        })
     

});

